import org.junit.Test;
import persistance.dao.DAO;
import persistance.dao.impl.GenericDAOImpl;
import persistance.models.Author;
import persistance.models.Book;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by mkory on 10/22/2015.
 */
public class AuthorTest {
    DAO<Author> authorDAO = new GenericDAOImpl<Author>();
    DAO<Book> bookDAO = new GenericDAOImpl<Book>();
    Author author = new Author();
    Set<Author> authorsOfTheBook;
    Author author2 = new Author();


//CRUD

    @Test
    public void createTest() {
        author.setName("Noam");
        authorDAO.createObject(author);
        author2.setName("Phillip");
        authorDAO.createObject(author2);
        authorsOfTheBook = new HashSet<Author>();
        authorsOfTheBook.add(author);
        authorsOfTheBook.add(author2);
        Book book = new Book("Gone with the wind", authorsOfTheBook, "200",1, Book.BookType.MAGAZIN);
        bookDAO.createObject(book);


        //delete update
    }

    @Test
    public void removeTest(){
       authorDAO.deleteObject(1,Author.class);
       // bookDAO.deleteObject(1,Book.class);
        //bookDAO.deleteObject(2,Book.class);
    //author and book has to be deleted togeather mapped ???
    }

    @Test
    public void updateTest(){
        Author author = authorDAO.readObjectById(6, Author.class);
        author.setName("Hello");
        authorDAO.updateObject(author);

    }

}


