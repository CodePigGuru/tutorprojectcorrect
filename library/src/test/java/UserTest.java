import junit.framework.Assert;
import org.junit.Test;
import persistance.dao.DAO;
import persistance.dao.impl.GenericDAOImpl;
import persistance.models.User;
import services.UserService;

/**
 * Created by mkory on 11/1/2015.
 */
public class UserTest {

    User user = new User();
    DAO<User> userDAO = new GenericDAOImpl<User>();
    UserService userService = new UserService();
    @Test
    public void createUser(){
        user.setName("Ivan");
        user.setPassword("111");
        userDAO.createObject(user);
    }

    @Test
    public void login(){
        Assert.assertEquals(true, userService.tryLogin("Ivan", "111"));

    }

    @Test
    public void register(){
        String username = "Nick";
        userService.registerUser(username,"234");

        User user = userDAO.readObjectByName(username, User.class);
        Assert.assertNotNull(user);
    }

}
