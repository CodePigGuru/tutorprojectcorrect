package services;

import persistance.dao.DAO;
import persistance.dao.impl.GenericDAOImpl;
import persistance.models.Author;
import persistance.models.Book;

import java.util.Set;

/**
 * Created by mkory on 11/1/2015.
 */
public class AuthorService {
    DAO<Author> authorDAO = new GenericDAOImpl<Author>();
    DAO<Book> bookDAO = new GenericDAOImpl<Book>();
    Author author;
    Book book;

    //incorrect behavior null pointer exception!
    public void deleteAuthor(int id){
        author = authorDAO.readObjectById(author.getId(),Author.class);
        Set<Book> booksWritten = author.getBooksWritten();

        for (Book book1 : booksWritten) {
            bookDAO.deleteObject(book1.getId(),Book.class);
        }

        authorDAO.deleteObject(author.getId(),Author.class);
    }
    /*
    Add author should add author to Database
    Add author should also add books written by author to DB into the filed books
     */
    public void addAuthor(String name, Set<Book> booksWritten){
        Author author = new Author(name,booksWritten);
        booksWritten = author.getBooksWritten();
        authorDAO.createObject(author);
        for (Book book : booksWritten){
            System.out.println("Created" + book.toString());
            bookDAO.createObject(book);
        }
    }


    }
