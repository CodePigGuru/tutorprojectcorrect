package services;

import persistance.dao.DAO;
import persistance.dao.impl.GenericDAOImpl;
import persistance.models.Author;
import persistance.models.Book;

import java.util.Set;

/**
 * Created by Mikhail on 11/3/2015.
 */
public class BookService {
    DAO<Book> bookDAO = new GenericDAOImpl<Book>();

    public void addBook(String name, Set<Author> authors,String year,int count,Book.BookType bookType){
        Book book = new Book(name,authors,year,count, bookType);
        bookDAO.createObject(book);
    }

}
