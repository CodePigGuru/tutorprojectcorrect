package services;

import persistance.dao.DAO;
import persistance.dao.impl.GenericDAOImpl;
import persistance.models.Book;
import persistance.models.User;

/**
 * Created by mkory on 11/1/2015.
 */
public class UserService {
    DAO<User> userDAO = new GenericDAOImpl<User>();
    DAO<Book> bookDAO = new GenericDAOImpl<Book>();


    public boolean tryLogin(String username, String password) {
        User user;
        user = userDAO.readObjectByName(username, User.class);
        if (user.getName().equals(username) && user.getPassword().equals(password)) {
            return true;
        }
        return false;
    }

    public int registerUser(String username, String password){
        User user = new User(username,password);
        userDAO.createObject(user);
        return userDAO.createObject(new User(username, password)).getId();
    }

    public boolean assignBookToUser(Book book, String username, String password){
        User user;
        user = userDAO.readObjectByName(username, User.class);
        if (bookDAO.readObjectById(book.getId(),Book.class) != null) {
            user.setBook(book);
            userDAO.updateObject(user);
            return true;
        }
        System.out.println("No such book");
        return false;
    }

    public boolean deleteUser(int id, String username){
        User user;
        user = userDAO.readObjectByName(username, User.class);
        if (user != null){
            userDAO.deleteObject(id,User.class);
            System.out.println("User is deleted");
            return true;
        }
        else {
            System.out.println("No such user");
            return false;
        }
    }

}
