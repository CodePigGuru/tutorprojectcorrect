package persistance.utils;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import javax.imageio.spi.ServiceRegistry;


/**
 * Created by mkory on 10/31/2015.
 */
public class HibernateService {
    private static final SessionFactory SESSION_FACTORY;

    static {
        try{
            SESSION_FACTORY = new Configuration().configure().buildSessionFactory();

        }catch (Throwable th){
            throw new ExceptionInInitializerError();
        }
    }

    public static Session getSession(){
        return SESSION_FACTORY.openSession();
    }

    public static SessionFactory getSessionFactory() {
        return SESSION_FACTORY;
    }
}
