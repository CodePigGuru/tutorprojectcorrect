package persistance.models;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by mkory on 10/18/2015.
 */
@Entity
public class User {

    @Id
    @GeneratedValue
    private int id;

    private String name;
    private String password;
    @OneToOne
    Book book = new Book();
    @OneToMany
    Set<Book> books = new HashSet<Book>();

    public Set<Book> getBooks() {
        return books;
    }

    public void setBooks(Set<Book> books) {
        this.books = books;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public User(String name, String password, Set<Book> books) {
        this.name = name;
        this.password = password;
        this.books = books;
    }


    public User(){

    }

    public User(String name, String password) {
        this.name = name;
        this.password = password;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
