package persistance.models;

import com.sun.javafx.beans.IDProperty;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by mkory on 10/18/2015.
 */
@Entity
public class Author {



    @Id
    @GeneratedValue
    private int id;
    private String name;

    @ManyToMany(mappedBy = "author")
    private Set<Book> booksWritten = new HashSet<Book>();

    public Author(String name, Set<Book> booksWritten) {
        this.name = name;
        this.booksWritten = booksWritten;
    }

    public Author() {

    }

    public Set<Book> getBooksWritten() {
        return booksWritten;
    }

    public void setBooksWritten(Set<Book> booksWritten) {
        this.booksWritten = booksWritten;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
