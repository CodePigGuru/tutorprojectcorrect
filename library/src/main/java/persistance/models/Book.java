package persistance.models;

import org.hibernate.validator.constraints.Email;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Set;

/**
 * Created by mkory on 10/18/2015.
 */
//?????????? hibernate ??????? ????? ????????? ? ??????? ???
@Entity //CHECk the status of the entity
@Table(name = "books")
public class Book {
    @Id
    @GeneratedValue
    private int id;

    private String name;

    @ManyToMany()
    private Set<Author> author;

    private String year;
    private int count;

    @Enumerated(EnumType.STRING)
    private BookType bookType;

    public enum BookType{
        MAGAZIN,
        JOURNAL,
        COVERBOOK
    };

    public Book(String name, Set<Author> author, String year, int count, BookType bookType) {
        this.name = name;
        this.author = author;
        this.year = year;
        this.count = count;
        this.bookType = bookType;
    }

    public Book(String name, String year, int count) {
        this.name = name;
        this.year = year;
        this.count = count;
    }

    public Book() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Author> getAuthor() {
        return author;
    }

    public void setAuthor(Set<Author> authorOfTheBook) {
        this.author = author;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public int getOcunt() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
