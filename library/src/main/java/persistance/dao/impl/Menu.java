package persistance.dao.impl;

import persistance.dao.DAO;
import persistance.models.Author;
import persistance.models.Book;
import persistance.models.User;
import services.AuthorService;
import services.UserService;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.Reader;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

/**
 * Created by Mikhail on 11/3/2015.
 */
public class Menu extends JFrame implements Runnable, ActionListener{
    Set<Book> booksWritten = new HashSet<Book>();
    DAO<Author> authorDAO = new GenericDAOImpl<Author>();
    AuthorService authorService = new AuthorService();
    private Button enterAuthorLibrary;
    private Button enterBookLibrary;
    private Button enterUserLibrary;
    JFrame menuFrame = new JFrame("Main Menu");
    MenuAuthor menuAuthor;
    public Menu () {
        menuFrame.setLayout(new FlowLayout());
        // "super" Frame sets its layout to FlowLayout, which arranges the components
        //  from left-to-right, and flow to next row from top-to-bottom.

                         // "super" Frame adds Label

             // set to read-only
        enterAuthorLibrary = new Button("Enter Author Library");
        menuFrame.add(enterAuthorLibrary);                     // "super" Frame adds tfCount

        enterBookLibrary = new Button("Enter Book Library");
        menuFrame.add(enterBookLibrary);

        enterUserLibrary = new Button("Enter User Library");
        menuFrame.add(enterUserLibrary);

        enterAuthorLibrary.addActionListener(this);
        enterBookLibrary.addActionListener(this);
        enterUserLibrary.addActionListener(this);
        // Clicking Button source fires ActionEvent
        // btnCount registers this instance as ActionEvent listener

        menuFrame.setTitle("Main Menu");  // "super" Frame sets title
        menuFrame.setSize(500, 500);        // "super" Frame sets initial window size


        // System.out.println(this);
        // System.out.println(lblCount);
        // System.out.println(tfCount);
        // System.out.println(btnCount);

        menuFrame.setVisible(true);
    }
    public void run() {
        int choice = 0;
        Scanner scanner = new Scanner(System.in);
        System.out.println("**********************");
        System.out.println("Welcome to the Library");
        System.out.println("***********************");
        System.out.println("Please select one of the OPTIONS");
        System.out.println("1 - Author library");
        System.out.println("2 - Book library");
        System.out.println("3 - User library");
        choice = scanner.nextInt();
        switch (choice){
            //Action that we can perform on user are here
            case 1 :
                System.out.println("You are in author library");
                int libraryMenuChoice = 0;
                System.out.println("Select what you want to do");
                System.out.println("1 - Add author");
                System.out.println("2 - Delete author");
                System.out.println("3 - Update author");
                System.out.println("4 - go to main menu");
                libraryMenuChoice = scanner.nextInt();
                switch (libraryMenuChoice){
                    //case 1 for entering info for author
                    case 1:
                        System.out.println("You want to add author");
                        String authorName;
                        System.out.print("Please enter author name");
                        authorName = scanner.next();
                        String bookName;
                        System.out.print("Enter the name of the book written");
                        bookName = scanner.next();
                        String bookYear;
                        System.out.print("Enter when the book was written");
                        bookYear = scanner.next();
                        Book book = new Book(bookName,bookYear,20);
                        booksWritten.add(book);
                        Author author = new Author(authorName,booksWritten);
                        authorService.addAuthor(author.getName(),author.getBooksWritten());
                        break;
                    //case two for deleting author from db
                    case 2:
                        System.out.println("You want to delete author");
                        int id;
                        System.out.println("Please enter id of author you want to delete");

                        authorDAO.deleteObject(1, Author.class);
                        break;
                }
                break;
            //Actions that we can perform on book are here
            case 2 :
                System.out.println("You are in book library");
                break;
            case 3 :
                System.out.println("You are in Author library");
                break;
            default:
                System.out.println("Invalid input");
                break;
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JFrame frame = new JFrame();
        Object src = e.getSource();
        if(src == enterAuthorLibrary){
            MenuAuthor menuAuthor = new MenuAuthor();
            frame.setVisible(false);
        }
        else if (src == enterBookLibrary){
            System.out.println("enterBook");
            
        }
        else if (src == enterUserLibrary){
            System.out.println("enterUser");
        }
    }
}

