package persistance.dao.impl;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import persistance.dao.DAO;
import persistance.utils.HibernateService;

import java.util.Collection;
import java.util.List;

/**
 * Created by dan on 1.6.15.
 */
public class GenericDAOImpl<T> implements DAO<T> {

    private SessionFactory sessionFactory = HibernateService.getSessionFactory();

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public T createObject(T t) {

        Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.save(t);
        session.getTransaction().commit();
        session.close();
        return t;
    }

    public T readObjectById(int id, Class<T> tClass) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();


        T t = (T) session
                .createQuery("from " + tClass.getSimpleName() + " where id = :itemId")
                .setLong("itemId", id).uniqueResult();

        session.getTransaction().commit();
        session.close();
        return t;
    }

    public T readObjectByUsername(String username, Class<T> tClass) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();

        T t = (T) session
                .createQuery("from " + tClass.getSimpleName() + " where username = :username")
                .setString("username", username).uniqueResult();

        session.getTransaction().commit();
        session.close();
        return t;
    }

    public T readObjectByName(String name, Class<T> tClass) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();

        T t = (T) session
                .createQuery("from " + tClass.getSimpleName() + " where name = :name")
                .setString("name", name).uniqueResult();
        session.getTransaction().commit();
        session.close();
        return t;
    }

    public boolean updateObject(T t) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();

        session.update(t);
        session.getTransaction().commit();
        session.close();
        return true;
    }

    public boolean deleteObject(int id, Class<T> tClass) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();

        T t = (T) session.load(tClass, id);
        session.delete(t);
        session.getTransaction().commit();
        session.close();
        return false;
    }

    public boolean isObjectInDatabase(int id, Class<T> tClass) {
        if (this.readObjectById(id, tClass) == null)
            return false;
        else
            return true;
    }

    public Collection<T> getAllObjects(Class<T> tClass) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();

        List objects = session.createQuery("from " + tClass.getSimpleName()).list();

        session.close();
        return objects;
    }
}
