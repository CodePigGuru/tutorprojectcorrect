package persistance.dao.impl;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Mikhail on 11/4/2015.
 */
public class MenuAuthor extends JFrame implements Runnable,ActionListener {
    MenuAuthor menuAuthor;
    private Button addAuthor;
    private Button deleteAutthor;
    private Button updateAuthor;
    public MenuAuthor () {
        JFrame frame = new JFrame("Author Library");
        frame.setDefaultCloseOperation(frame.EXIT_ON_CLOSE);
        frame.setLayout(new FlowLayout());
        // "super" Frame sets its layout to FlowLayout, which arranges the components
        //  from left-to-right, and flow to next row from top-to-bottom.

        // "super" Frame adds Label

        // set to read-only
        addAuthor = new Button("Click to add Author");
        frame.add(addAuthor);                     // "super" Frame adds tfCount

        deleteAutthor = new Button("Click to delete Author");
        frame.add(deleteAutthor);

        updateAuthor = new Button("Click to update author");
        frame.add(updateAuthor);

        addAuthor.addActionListener(this);
        deleteAutthor.addActionListener(this);
        updateAuthor.addActionListener(this);
        // Clicking Button source fires ActionEvent
        // btnCount registers this instance as ActionEvent listener

        frame.setTitle("Author library");  // "super" Frame sets title
        frame.setSize(500, 500);        // "super" Frame sets initial window size


        // System.out.println(this);
        // System.out.println(lblCount);
        // System.out.println(tfCount);
        // System.out.println(btnCount);

        frame.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
    MenuAuthor menuAuthor = new MenuAuthor();
    }

    @Override
    public void run() {

    }
}
