import dan.ChainItem;

/**
 * Created by mkory on 10/11/2015.
 */
public class Application implements ChainItem {
    Message message = new Message();

    String application = "From Sarah>" + message.getName();

    public String multiplex() {
        System.out.println(application);
        return application;
    }

    public String demultiplex(Message message) {
        return null;
    }

    public String multiplex(String message) {
        return null;
    }

    public void demultiplex(String message) {

    }

    public void setNexFilter(ChainItem abstractTCP) {

    }

}
