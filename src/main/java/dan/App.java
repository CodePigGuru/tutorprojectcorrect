package dan;

/**
 * Created by mkory on 10/11/2015.
 */

// HW TODO demultiplex + info
public class App {
    public static void main(String[] args) {
        ChainItem applicationFilter = new ApplicationFilter();
        ChainItem transportFilter = new TransportFilter();
        ChainItem internetFilnter = new InternetFilter();
        ChainItem linkFilter = new LinkFilter();

        applicationFilter.setNexFilter(transportFilter);
        transportFilter.setNexFilter(internetFilnter);
        internetFilnter.setNexFilter(linkFilter);
        applicationFilter.multiplex("Hey Joel what's up?");

    }
}
