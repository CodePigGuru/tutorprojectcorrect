package dan;

/**
 * Created by mkory on 10/11/2015.
 */
public interface ChainItem {

    public String multiplex(String message);

    public void demultiplex(String message);

    public void setNexFilter(ChainItem abstractTCP);

}
