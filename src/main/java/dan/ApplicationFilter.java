package dan;

/**
 * Created by mkory on 10/11/2015.
 */
public class ApplicationFilter implements ChainItem {
    ChainItem nextChainItem;

    public String multiplex(String message) {
        message = "From Sarah >" + message;
        System.out.println("Application: "+message);
        if(nextChainItem!=null){
            nextChainItem.multiplex(message);
        }
        return message;
    }

    public void demultiplex(String message) {
    }

    public void setNexFilter(ChainItem nextChainItem) {
    this.nextChainItem = nextChainItem;
    }

}
