package dan;

/**
 * Created by mkory on 10/11/2015.
 */
public class LinkFilter implements ChainItem {
    ChainItem nextChainItem;

    public String multiplex(String message) {
        message = "send" + "(\"" + "MAC1:" + message;
        message += "\"";
        System.out.println("Link:" + message);
        if (nextChainItem != null) {
            nextChainItem.multiplex(message);
        }
        return message;
    }

    public void demultiplex(String message) {

    }

    public void setNexFilter(ChainItem abstractTCP) {
        this.nextChainItem = abstractTCP;
    }
}
