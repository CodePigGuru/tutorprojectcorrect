package app;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

/**
 * Created by mkory on 11/1/2015.
 */
public class Appliaction {

    public static void main(String[] args) {
        User user = new User(1, "Hi", "123");
        String contextPath = "file:C:\\Users\\mkory\\IdeaProjects\\springbean\\web\\WEB-INF\\applicationContext.xml";
        ApplicationContext context = new FileSystemXmlApplicationContext(contextPath);

        User testUser = (User)context.getBean("testUser");
        System.out.println(testUser);
        User testUser1 = (User) context.getBean("nextUser");
        System.out.println("testUser1 = " + testUser1);


    }


}
