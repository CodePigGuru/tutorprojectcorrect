package patterns.observers;

import patterns.Observer;
import patterns.Subject;

/**
 * Created by mkory on 10/31/2015.
 */
public class HexObserver extends Observer implements TE {

    @Override
    public void update() {
        System.out.println("Hex string:" + Integer.toHexString(subject.getValue()).toUpperCase());
    }

    public HexObserver(Subject subject) {
        super.subject = subject;
    }

    public void setA(int value) {

    }
}
