package patterns.observers;
import patterns.Observer;
import patterns.Subject;

/**
 * Created by mkory on 10/31/2015.
 */
public class BinaryObserver extends Observer {
    @Override
    public void update() {
        System.out.println("Binary string:" + Integer.toBinaryString(subject.getValue()));
    }

    public BinaryObserver(Subject subject) {
        super.subject = subject;
    }
}
