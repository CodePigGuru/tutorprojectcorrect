package patterns.observers;

import patterns.Subject;

/**
 * Created by mkory on 10/31/2015.
 */
public class Application {

    public static void main(String[] args) {
        Subject subject = new Subject();
        BinaryObserver binaryObserver = new BinaryObserver(subject);
        HexObserver hexObserver = new HexObserver(subject);
        subject.attachObserver(hexObserver);
        subject.attachObserver(binaryObserver);
        subject.setValue(1);
        System.out.println("**************");
        subject.setValue(10);
        System.out.println("***************");
        subject.setValue(13);
    }
}
