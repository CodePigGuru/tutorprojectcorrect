package patterns;

/**
 * Created by mkory on 10/31/2015.
 */
public abstract class Observer {
    public Subject subject;

    public abstract void update();
}
