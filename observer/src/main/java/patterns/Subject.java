package patterns;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mkory on 10/31/2015.
 */
public class Subject {

    private int value;
    List<Observer> observerList = new ArrayList<Observer>();


    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
        notifyAllObservers();
    }

    public List<Observer> getObserverList() {
        return observerList;
    }

    public void setObserverList(List<Observer> observerList) {
        this.observerList = observerList;
    }

    public void attachObserver(Observer observer) {
        observerList.add(observer);
    }

    public void notifyAllObservers() {
        for (Observer observer : observerList) {
            observer.update();
        }
    }


}
